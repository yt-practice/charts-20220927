import {
	BarChart,
	BarSeriesOption,
	CustomChart,
	CustomSeriesOption,
	LineChart,
	LineSeriesOption,
	PieChart,
	PieSeriesOption,
	ScatterChart,
	ScatterSeriesOption,
} from 'echarts/charts'
import {
	DatasetComponent,
	DatasetComponentOption,
	GridComponent,
	GridComponentOption,
	LegendComponent,
	LegendComponentOption,
	TitleComponent,
	TitleComponentOption,
	TooltipComponent,
	TooltipComponentOption,
	TransformComponent,
	VisualMapComponent,
	VisualMapComponentOption,
} from 'echarts/components'
import type { ComposeOption } from 'echarts/core'
import * as echarts from 'echarts/core'
import { LabelLayout } from 'echarts/features'
import { SVGRenderer } from 'echarts/renderers'

export type ECOption = ComposeOption<
	| BarSeriesOption
	| ScatterSeriesOption
	| PieSeriesOption
	| LineSeriesOption
	| CustomSeriesOption
	| TitleComponentOption
	| DatasetComponentOption
	| GridComponentOption
	| TooltipComponentOption
	| VisualMapComponentOption
	| LegendComponentOption
>

echarts.use([BarChart, ScatterChart, PieChart, LineChart, CustomChart])

echarts.use([
	TitleComponent,
	GridComponent,
	DatasetComponent,
	TooltipComponent,
	VisualMapComponent,
	TransformComponent,
	LegendComponent,
])
echarts.use(LabelLayout)
echarts.use(SVGRenderer)

export { echarts }
