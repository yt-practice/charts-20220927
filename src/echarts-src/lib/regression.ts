const fixed = (num: number) => Math.round(num * 100) / 100

type Point = [x: number, y: number]
type Data = readonly (readonly [x: number, y: number, w: number])[]

export const linear = (data: Data) => {
	let sxy = 0
	let sx2 = 0
	let sy2 = 0
	let sk = 0
	let sx = 0
	let sy = 0
	for (const [x, y, w0] of data) {
		const w = w0 <= 0 || !Number.isFinite(w0) ? 1 : w0
		sk += w
		sx += w * x
		sy += w * y
		sx2 += w * x * x
		sy2 += w * y * y
		sxy += w * x * y
	}

	const tmp = sx2 - (sx * sx) / sk
	const gradient = 0 === tmp ? 0 : (sxy - (sx * sy) / sk) / tmp

	const intercept = sy / sk - (gradient * sx) / sk

	const ar = (sx2 - (sx * sx) / sk) * (sy2 - (sy * sy) / sk)
	const coefficient = 0 < ar ? (sxy - (sx * sy) / sk) / ar ** (1 / 2) : 0

	const points = data
		.map(([x]): Point => [x, gradient * x + intercept])
		.sort((q, w) => q[0] - w[0])

	const parameter = { gradient, intercept }
	const expression = `y = ${fixed(gradient)}x + ${fixed(intercept)}`

	return { points, parameter, expression, coefficient }
}
