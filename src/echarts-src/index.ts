import { colors } from './colors'
import { echarts, ECOption } from './echarts'
import { columnOption } from './types/column'
import { customOption } from './types/custom'
import { nestedPieOption } from './types/nested-pie'
import { pieOption } from './types/pie'
import { regressionOption } from './types/regression'
import { scatterOption } from './types/scatter'
import { stackedBarOption } from './types/stacked-bar'
import {
	stackedBar2NoDataOption,
	stackedBar2Option,
} from './types/stacked-bar-2'

const chartDom = document.createElement('main')
document.body.appendChild(chartDom)

const chart = echarts.init(chartDom, void 0, { height: 300, renderer: 'svg' })
window.addEventListener('resize', () => {
	chart.resize()
})
const option: ECOption = {
	xAxis: {
		type: 'category',
		data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
	},
	yAxis: {
		type: 'value',
	},
	series: [
		{
			data: [120, 200, 150, 80, 70, 110, 130],
			type: 'bar',
			showBackground: true,
			backgroundStyle: {
				color: 'rgba(180, 180, 180, 0.2)',
			},
		},
	],
}

type Tab = Readonly<{
	name: string
	option?: ECOption
}>
;(() => {
	const tabs: readonly Tab[] = [
		{ name: 'テスト', option },
		{ name: '帯', option: stackedBarOption },
		{ name: '点' }, // 多分 scatter で実装
		{ name: '散布図', option: scatterOption },
		{ name: '円', option: pieOption },
		{ name: '二重円', option: nestedPieOption },
		{ name: '比較縦棒', option: columnOption },
		{ name: '折れ線' },
		{ name: '回帰分析', option: regressionOption },
		{ name: 'レーダーチャート' },
		{ name: '積み重ね棒' },
		{ name: 'バランス', option: customOption },
		{ name: '両軸' },
		{ name: 'ＸＹ' },
		{ name: '工程管理' },
		{ name: '散布と線' },
		{ name: '余白なし帯', option: stackedBar2Option },
		{ name: '余白なしnodata', option: stackedBar2NoDataOption },
	]
	chart.setOption(tabs.find(t => '余白なし帯' === t.name)?.option || option)
	chart.setOption({ color: colors.slice() }, { notMerge: false })
	document.body.appendChild(document.createElement('hr'))
	const tabsWrap = document.createElement('div')
	document.body.appendChild(tabsWrap)
	for (const { name, option } of tabs) {
		const button = document.createElement('button')
		button.textContent = name
		tabsWrap.appendChild(button)
		if (!option) button.disabled = true
		else
			button.addEventListener('click', () => {
				chart.setOption(option, { notMerge: true })
			})
	}
})()
