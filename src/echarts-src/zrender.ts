import * as zrender from 'zrender'
import Painter from 'zrender/lib/svg/Painter'

import { colors } from './colors'

zrender.registerPainter('svg', Painter)

console.log('hogehoge!!!')

const cssAssign = (
	el: { readonly style: CSSStyleDeclaration },
	st: Partial<CSSStyleDeclaration>,
) => Object.assign(el.style, st)

const root = document.createElement('main')
cssAssign(root, {
	width: '400px',
	height: '300px',
	border: '1px solid red',
})
document.body.appendChild(root)

const opts = { duration: 550, easing: 'quadraticInOut' } as const

const setToggleColor = (rect: zrender.Rect | zrender.Text) => {
	let c = -1
	rect.onclick = () => {
		const fill = colors[(c = (c + 1) % colors.length)]
		rect.animateTo({ style: { fill } }, opts)
	}
}

let zr: ReturnType<typeof zrender.init> | null = null
const render = () => {
	zr = zrender.init(root, { renderer: 'svg' })

	const rect1 = new zrender.Rect({
		shape: { x: 100, y: 100, width: 130, height: 150 },
	})
	rect1.animateFrom({ shape: { height: 0 } }, opts)
	const rect2 = new zrender.Rect({
		shape: { x: 230, y: 0, width: 130, height: 100 },
	})
	rect2.animateFrom({ shape: { height: 0 } }, opts)
	zr.add(rect1)
	zr.add(rect2)

	setToggleColor(rect1)
	setToggleColor(rect2)

	for (let i = 0; i < 3; ++i) {
		const txt = new zrender.Text({
			style: {
				text: 'hogehoge',
				x: 10 + i * 16,
				y: 10 + i * 16,
			},
		})
		zr.add(txt)
		setToggleColor(txt)
	}
}

render()

document.body.appendChild(document.createElement('hr'))
const label = document.createElement('label')
const toggle = document.createElement('input')
toggle.type = 'checkbox'
toggle.checked = !!zr
cssAssign(label, { padding: '10px', marginLeft: '5px', userSelect: 'none' })
label.appendChild(toggle)
label.appendChild(document.createTextNode('rendered'))
document.body.appendChild(label)
toggle.addEventListener('change', e => {
	e.preventDefault()
	if (zr) {
		zrender.dispose(zr)
		zr = null
	} else {
		render()
	}
})
