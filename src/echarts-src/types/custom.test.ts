import { withDepth } from './custom'

// const item = (name: string, value: number) => ({ name, value })

describe('withDepth', () => {
	const list: {
		ret: ReturnType<typeof withDepth>
	}[] = [
		{
			ret: { name: 'hoge', value: 10, depth: 1 },
		},
		{
			ret: { name: 'a', value: 10, depth: 1, children: [] },
		},
		{
			ret: {
				name: 'a',
				value: 10,
				depth: 2,
				children: [
					{ name: 'b', value: 6, depth: 1 },
					{ name: 'c', value: 4, depth: 1 },
				],
			},
		},
		{
			ret: {
				name: 'a',
				value: 10,
				depth: 4,
				children: [
					{ name: 'b', value: 6, depth: 1 },
					{
						name: 'c',
						value: 4,
						depth: 3,
						children: [
							{ name: 'd', value: 1, depth: 1 },
							{
								name: 'e',
								value: 3,
								depth: 2,
								children: [
									{ name: 'f', value: 1, depth: 1 },
									{ name: 'g', value: 2, depth: 1 },
								],
							},
						],
					},
				],
			},
		},
	]
	for (const { ret } of list) {
		it(JSON.stringify(ret), () => {
			expect(withDepth(ret, 0)).toEqual(ret)
		})
	}
})
