import type { ECOption } from '../echarts'

const barOpts = {
	barCategoryGap: 2,
	barGap: 0,
	barWidth: 22,
	barMaxWidth: 22,
} as const

export const stackedBar2Option: ECOption = {
	tooltip: {
		trigger: 'axis',
		axisPointer: {
			// Use axis to trigger tooltip
			type: 'shadow', // 'shadow' as default; can also be 'line' or 'shadow'
		},
	},
	legend: {
		show: true,
	},
	grid: {
		// left: '3%',
		// right: '4%',
		// bottom: '3%',
		left: 0,
		right: 0,
		bottom: 0,
		top: 24 * 3,
		height: 24 * (3 + 7),
		// height: 120,
		// containLabel: true,
	},
	xAxis: {
		type: 'value',
	},
	yAxis: {
		type: 'category',
		data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
	},
	series: [
		{
			name: 'Direct',
			type: 'bar',
			// barGap: 0,
			...barOpts,
			// barCategoryGap: 20,
			stack: 'total',
			label: {
				show: true,
			},
			emphasis: {
				focus: 'series',
			},
			data: [320, 302, 301, 334, 390, 330, 320],
		},
		{
			name: 'Mail Ad',
			type: 'bar',
			// barGap: 0,
			...barOpts,
			// barCategoryGap: 2,
			stack: 'total',
			label: {
				show: true,
			},
			emphasis: {
				focus: 'series',
			},
			data: [120, 132, 101, 134, 90, 230, 210],
		},
		{
			name: 'Affiliate Ad',
			type: 'bar',
			// barGap: 0,
			...barOpts,
			// barCategoryGap: 2,
			stack: 'total',
			label: {
				show: true,
			},
			emphasis: {
				focus: 'series',
			},
			data: [220, 182, 191, 234, 290, 330, 310],
		},
		{
			name: 'Video Ad',
			type: 'bar',
			// barGap: 0,
			...barOpts,
			// barCategoryGap: 2,
			stack: 'total',
			label: {
				show: true,
			},
			emphasis: {
				focus: 'series',
			},
			data: [150, 212, 201, 154, 190, 330, 410],
		},
		{
			name: 'Search Engine',
			type: 'bar',
			// barGap: 0,
			...barOpts,
			// barCategoryGap: 2,
			// barGap: 0,
			// barWidth: 22,
			stack: 'total',
			label: {
				show: true,
			},
			emphasis: {
				focus: 'series',
			},
			data: [820, 832, 901, 934, 1290, 1330, 1320],
		},
	],
}

export const stackedBar2NoDataOption: ECOption = {
	...stackedBar2Option,
	legend: {
		show: true,
		// @ts-expect-error: ignore
		// eslint-disable-next-line
		data: stackedBar2Option.series.map(s => s.name as string) as string[],
	},
	grid: { height: 0, containLabel: false },
	xAxis: { type: 'value' },
	yAxis: { type: 'category' },
	// @ts-expect-error: ignore
	// eslint-disable-next-line
	series: stackedBar2Option.series.map(s => ({
		// eslint-disable-next-line
		name: s.name as string,
		type: 'bar',
	})),
}
