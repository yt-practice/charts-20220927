// import ecStat from 'echarts-stat'

import type { ECOption } from '../echarts'
import { linear } from '../lib/regression'

const data: [number, number, number][] = [
	[40, 115, 8000],
	[45, 95, 3000],
	[5, 100, 200],
	[25, 80, 6500],
]

const myRegression = linear(data)
// console.log(myRegression)

export const regressionOption: ECOption = {
	tooltip: {
		trigger: 'axis',
		axisPointer: {
			type: 'cross',
		},
	},
	xAxis: {
		type: 'value',
		splitLine: {
			lineStyle: {
				type: 'dashed',
			},
		},
	},
	yAxis: {
		type: 'value',
		min: data.reduce((q, w) => Math.min(w[1], q), 1 / 0),
		// min: 1.5,
		splitLine: {
			lineStyle: {
				type: 'dashed',
			},
		},
	},
	series: [
		{
			name: 'scatter',
			type: 'scatter',
			emphasis: {
				label: {
					show: true,
				},
			},
			data,
		},
		{
			name: 'line',
			type: 'line',
			showSymbol: false,
			data: myRegression.points,
			markPoint: {
				itemStyle: {
					color: 'transparent',
				},
				label: {
					show: true,
					formatter: myRegression.expression,
				},
				data: [
					{
						name: 'points',
						coord: myRegression.points[myRegression.points.length - 1],
					},
				],
			},
		},
	],
}
