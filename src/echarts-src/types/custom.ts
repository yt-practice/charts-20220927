/* eslint-disable @typescript-eslint/no-non-null-assertion */

import type { CustomSeriesRenderItemReturn } from 'echarts/types/dist/shared'

import type { ECOption } from '../echarts'

export type Data = Readonly<{
	name: string
	value: number
	children?: readonly Data[]
}>
const data: Data = {
	name: '売上',
	value: 2000,
	children: [
		{ name: '原価', value: 1200 },
		{
			name: '粗利益',
			value: 800,
			children: [
				{ name: '固定費', value: 650 },
				{ name: '利益', value: 150 },
			],
		},
	],
}

type Simple = Readonly<{
	name: string
	value: number
	x: number
	y: number
	w: number
	h: number
}>

type DataWithDepth = Readonly<{
	name: string
	value: number
	depth: number
	children?: readonly DataWithDepth[]
}>

export const withDepth = (data: Data, depth: number): DataWithDepth => {
	if (!data.children?.length)
		return { ...data, depth: 1 + depth } as DataWithDepth
	const children = data.children.map(d => withDepth(d, depth))
	const d = Math.max(...children.map(d => d.depth))
	return { ...data, depth: 1 + d, children }
}

const formatData = (data: Data) => {
	const item = withDepth(data, 0)
	type DD = DataWithDepth
	const flatten: Simple[] = []
	const walk = (data: DD, p0: Simple): void => {
		if (data.children) {
			const x = p0.x + p0.w
			let y = p0.y
			const r = 1 - x
			for (const child of data.children) {
				const w = r / child.depth
				const h = p0.h * (child.value / p0.value)
				const c0 = { name: child.name, value: child.value, x, y, h, w }
				flatten.push(c0)
				walk(child, c0)
				y += h
			}
		}
	}
	const p0 = {
		name: item.name,
		value: item.value,
		x: 0,
		y: 0,
		h: 1,
		w: 1 / item.depth,
	}
	flatten.push(p0)
	walk(item, p0)
	return { flatten }
}

const formated = formatData(data)

export const customOption: ECOption = {
	title: {
		text: 'custom',
		left: 'center',
	},
	series: [
		{
			type: 'custom',
			coordinateSystem: 'none',
			data: formated.flatten,
			colorBy: 'data',
			renderItem: (params, api) => {
				const item = formated.flatten[params.dataIndex]!
				const rw = api.getWidth()
				const rh = api.getHeight()
				const x = rw * item.x
				const y = rh * item.y
				const width = rw * item.w
				const height = rh * item.h
				return {
					type: 'rect',
					shape: { x, y, width, height },
					style: api.style(),
					textContent: {
						style: { text: `${item.name}\n${item.value}` },
					},
				} as CustomSeriesRenderItemReturn
			},
		},
	],
}
