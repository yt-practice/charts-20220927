import { BarChart } from 'chartist'

// eslint-disable-next-line no-new
new BarChart(
	'#chart',
	{
		labels: ['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10'],
		series: [[1, 2, 4, 8, 6, -2, -1, -4, -6, -2]],
	},
	{
		high: 10,
		low: -10,
		axisX: {
			labelInterpolationFnc: (value, index) => (0 === index % 2 ? value : null),
		},
	},
)
