import type { ApexOptions } from 'apexcharts'

/**
 * 円グラフ
 *
 * 二重円グラフはドーナツグラフを重ねることになりそう
 */
export const simplePieOptions: ApexOptions = {
	series: [44, 55, 13, 43, 22],
	chart: {
		width: 380,
		type: 'pie',
	},
	labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
	responsive: [
		{
			breakpoint: 480,
			options: {
				chart: {
					width: 200,
				},
				legend: {
					position: 'bottom',
				},
			},
		},
	],
}
