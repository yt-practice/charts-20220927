import type { ApexOptions } from 'apexcharts'

export const xYLineOptions: ApexOptions = {
	chart: {
		height: 380,
		width: '100%',
		type: 'line',
	},
	series: [
		{
			name: 'Series 1',
			data: [
				[1, 34],
				[3.8, 43],
				[5, 31],
				[10, 43],
				[13, 33],
				[15, 43],
				[18, 33],
				[20, 52],
				[16, 38],
				[12, 45], // 戻れる
				[6, 40],
			],
		},
	],
	xaxis: {
		type: 'numeric',
	},
	tooltip: {
		x: {
			formatter(val) {
				return val.toFixed(1)
			},
		},
	},
}
