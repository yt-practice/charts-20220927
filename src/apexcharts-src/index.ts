import ApexCharts from 'apexcharts'
// import type CApexCharts from 'apexcharts'
import type { ApexOptions } from 'apexcharts'
// // @ts-expect-error: hoge
// import ApexCharts_ from 'apexcharts/src/apexcharts.js'

import { barWithMarkersOptions } from './bar-with-markers'
import { basicColumnOptions } from './basic-column'
import { distributedTimelineOptions } from './distributed-timeline'
import { distributedTreemapOptions } from './distributed-treemap'
import { lineColumnMixedOptions } from './line-column-mixed'
import { lineWithDataLabelsOptions } from './line-with-data-labels'
import { radarWithMultipleSeriesOptions } from './radar-with-multiple-series'
import { scatterOptions, scatterWithLineOptions } from './scatter'
import { series } from './series'
import { simpleBubbleOptions } from './simple-bubble'
import { simplePieOptions } from './simple-pie'
import { stackedBarOptions } from './stacked-bar'
import { stackedColumnsOptions } from './stacked-columns'
import { xYLineOptions } from './x-y-line'

// const ApexCharts = ApexCharts_ as typeof CApexCharts

const options: ApexOptions = {
	series: [
		{
			name: 'STOCK ABC',
			data: series.monthDataSeries1.prices,
		},
	],
	chart: {
		type: 'area',
		height: 350,
		zoom: {
			enabled: false,
		},
	},
	dataLabels: {
		enabled: false,
	},
	stroke: {
		curve: 'straight',
	},
	title: {
		text: 'Fundamental Analysis of Stocks',
		align: 'left',
	},
	subtitle: {
		text: 'Price Movements',
		align: 'left',
	},
	labels: series.monthDataSeries1.dates,
	xaxis: {
		type: 'datetime',
	},
	yaxis: {
		opposite: true,
	},
	legend: {
		horizontalAlign: 'left',
	},
}

;(async () => {
	const wrap = document.createElement('div')
	document.body.appendChild(wrap)

	const tabs = [
		{ name: 'テスト', options },
		{ name: '帯', options: stackedBarOptions },
		{ name: '点', options: barWithMarkersOptions },
		{ name: '散布図', options: scatterOptions },
		{ name: '円・二重円', options: simplePieOptions },
		{ name: '比較縦棒', options: basicColumnOptions },
		{ name: '折れ線', options: lineWithDataLabelsOptions },
		{ name: '回帰分析', options: simpleBubbleOptions },
		{ name: 'レーダーチャート', options: radarWithMultipleSeriesOptions },
		{ name: '積み重ね棒', options: stackedColumnsOptions },
		{ name: 'バランス', options: distributedTreemapOptions },
		{ name: '両軸', options: lineColumnMixedOptions },
		{ name: 'ＸＹ', options: xYLineOptions },
		{ name: '工程管理', options: distributedTimelineOptions },
		{ name: '散布と線', options: scatterWithLineOptions },
	]
	const tabsWrap = document.createElement('div')
	document.body.appendChild(tabsWrap)

	let chart = new ApexCharts(wrap, options)
	await chart.render()

	for (const tab of tabs) {
		const button = document.createElement('button')
		button.textContent = tab.name
		tabsWrap.appendChild(button)
		button.addEventListener('click', () => {
			chart.destroy()
			chart = new ApexCharts(wrap, tab.options)
			chart.render().catch(console.error)
		})
	}
})().catch(x => {
	console.error(x)
})
