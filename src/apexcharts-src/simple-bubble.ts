import type { ApexOptions } from 'apexcharts'

type Range = Readonly<{ min: number; max: number }>
type Data = ApexAxisChartSeries[number]['data']

const generateData = (count: number, yrange: Range): Data => {
	const series: [number, number, number][] = []
	for (let i = 0; i < count; ++i) {
		const x = Math.floor(Math.random() * (750 - 1 + 1)) + 1
		const y =
			Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min
		const z = Math.floor(Math.random() * (75 - 15 + 1)) + 15
		series.push([x, y, z])
	}
	return series as unknown as Data
}

/**
 * 回帰分析Ｉグラフ
 *
 * 重みを表すバブルチャートと直線回帰線を表示する
 *
 * 直線回帰線を乗せる必要がある
 */

export const simpleBubbleOptions: ApexOptions = {
	series: [
		{
			name: 'Bubble1',
			data: generateData(20, { min: 10, max: 60 }),
		},
		{
			name: 'Bubble2',
			data: generateData(20, { min: 10, max: 60 }),
		},
		{
			name: 'Bubble3',
			data: generateData(20, { min: 10, max: 60 }),
		},
		{
			name: 'Bubble4',
			data: generateData(20, { min: 10, max: 60 }),
		},
	],
	chart: {
		height: 350,
		type: 'bubble',
	},
	dataLabels: {
		enabled: false,
	},
	fill: {
		opacity: 0.8,
	},
	title: {
		text: 'Simple Bubble Chart',
	},
	xaxis: {
		tickAmount: 12,
		type: 'category',
	},
	yaxis: {
		max: 70,
	},
}
